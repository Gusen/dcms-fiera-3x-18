<?
include_once '../sys/inc/start.php';
include_once '../sys/inc/compress.php';
include_once '../sys/inc/sess.php';
include_once '../sys/inc/home.php';
include_once '../sys/inc/settings.php';
include_once '../sys/inc/db_connect.php';
include_once '../sys/inc/ipua.php';
include_once '../sys/inc/fnc.php';
include_once '../sys/inc/adm_check.php';
include_once '../sys/inc/user.php';
user_access('user_cab',null,'index.php?'.SID);
adm_check();
$set['title'] = lang('Управление кабинетом');
include_once H.'sys/inc/thead.php';
title();

$get_type = (@$_GET['get_type'] && @$_GET['get_type'] == 2 ? 2 : null);
$get_type_set = ($get_type == null ? 'index' : 'settings'); 

lang::start('default');







if (isset($_POST['add']))
{

$type_set = my_esc($_POST['type_set']);
$name= my_esc($_POST['name']);
$type= my_esc($_POST['type']);
$url= my_esc($_POST['url']);
$pos= my_esc($_POST['pos']);
$pos2= my_esc($_POST['pos2']);

if ($type_set == 'index')
$pos = mysql_result(query("SELECT MAX(`pos`) FROM `user_menu`"), 0)+1;
else
$pos = 0;

if ($type_set == 'settings')
$pos2 = mysql_result(query("SELECT MAX(`pos2`) FROM `user_menu`"), 0)+1;
else
$pos2 = 0;

query("INSERT INTO `user_menu` (`type_set`, `name`,`type`,`url`,`pos`,`pos2`) VALUES 
('$type_set','$name', '$type', '$url', '$pos', '$pos2')");

$_SESSION['message'] = lang('Ссылка успешно добавлена');
exit(header('Location: ?'));
 
}


if (isset($_POST['name'],$_POST['url'],$_POST['change'],$_GET['id']) && $_POST['name'] != NULL && $_POST['url'] != NULL)
{

$id = intval($_GET['id']);
$name = my_esc($_POST['name']);
$url = my_esc($_POST['url']);
$type = my_esc($_POST['type']);
$type_set = my_esc($_POST['type_set']);

query("UPDATE `user_menu` SET `name` = '$name', `url` = '$url', `type` = '$type', `type_set` = '$type_set' WHERE `id` = '$id' LIMIT 1");
$_SESSION['message'] = lang('Пункт меню успешно изменен');
exit(header('Location: ?'));
 
}

if (isset($_GET['id']) && isset($_GET['act']) && mysql_result(query("SELECT COUNT(*) FROM `user_menu` WHERE `id` = '".intval($_GET['id'])."'"),0))
{

$menu=mysql_fetch_assoc(query("SELECT * FROM `user_menu` WHERE `id` = '".intval($_GET['id'])."' LIMIT 1"));
$pos = 'pos'.$get_type;

if ($_GET['act']=='up')
{

query("UPDATE `user_menu` SET `$pos` = '".($menu[$pos])."' WHERE `$pos` = '".($menu[$pos]-1)."' LIMIT 1");
query("UPDATE `user_menu` SET `$pos` = '".($menu[$pos]-1)."' WHERE `id` = '".intval($_GET['id'])."' LIMIT 1");

$_SESSION['message'] = lang('Пункт меню сдвинут на позицию вверх');
exit(header('Location: ?get_type='.$get_type));
 
}
if ($_GET['act']=='down')
{

query("UPDATE `user_menu` SET `$pos` = '".($menu[$pos])."' WHERE `$pos` = '".($menu[$pos]+1)."' LIMIT 1");
query("UPDATE `user_menu` SET `$pos` = '".($menu[$pos]+1)."' WHERE `id` = '".intval($_GET['id'])."' LIMIT 1");

$_SESSION['message'] = lang('Пункт меню сдвинут на позицию вниз');
exit(header('Location: ?get_type='.$get_type));
}
if ($_GET['act']=='del')
{

query("DELETE FROM `user_menu` WHERE `id` = '".intval($_GET['id'])."' LIMIT 1");


$_SESSION['message'] = lang('Пункт меню удален');
exit(header('Location: ?get_type='.$get_type));
}


}
 
  

err();
aut();
echo (" <div class='p_m'> Сменить раздел на : <b>".($get_type == 2 ? "<a href='?get_type=1'> Кабинет</a>":"<a href='?get_type=2'>Настройки</a>") .'</b></div>');

echo "<table class='post'>";
$q=query("SELECT * FROM `user_menu` where `type_set` = '$get_type_set' ORDER BY `pos$get_type` ASC");
while ($post = mysql_fetch_assoc($q))
{
echo "   <tr>\n";
echo "  <td class='adm_panel'>";

if ($post['pos'.$get_type] <= 0)
{

	$pos = 'pos'.$get_type;
	$posI = mysql_result(query("SELECT MAX(`$pos`) FROM `user_menu`"), 0)+1;
	query("UPDATE `user_menu` SET `$pos` = '".($posI)."' WHERE `id` = '".$post['id']."' LIMIT 1");
}

//Выводим иконку
if ($post['type'] == 'razd') 
echo "<img src='/style/icons_admpanel/razd.png'> ";
//echo ($get_type  == null ? $post['pos']  : $post['pos2']);
echo  lang($post['name']) ." <span style='float:right' class='adm_panel_span'>  <a href='?id=$post[id]&amp;act=edit&get_type=$get_type'>".lang('Редактировать')." </a></span>";
echo "  </td>\n";
echo "   </tr>\n";
echo "   <tr>\n";
echo "  <td class='p_m'>";
echo output_text($post['url']).'<br/><br/>';
 

if (isset($_GET['id']) && $_GET['id']==$post['id'] && isset($_GET['act']) && $_GET['act']=='edit')
{

echo "<form action=\"?id=$post[id]\" method=\"post\">";

echo lang('Название')." :<br />";
echo "<input type='text' name='name' value='".lang($post['name'])."' /><br />";

echo lang('Ссылка на виджет')." :<br />";
echo "<input type='text' name='url' value='".($post['url'])."' /><br />";

echo "Тип: <br />";
echo "<select name='type'>";
echo "<option value='inc'> ".lang('Виджет')."</option>";
echo "<option value='razd'> ".lang('Раздел')."</option>";
echo "</select><br />";

echo "Раздел: <br />";
echo "<select name='type_set'>";
echo "<option value='index'> ".lang('Кабинет')."</option>";
echo "<option value='settings'> ".lang('Настройки')."</option>";
echo "</select><br />";



echo "<input class=\"submit\" name=\"change\" type=\"submit\" value='".lang('Изменить')."' /><br />\n";
echo "</form>";


echo "<a href='?'>".lang('Отмена')."</a><br />";
}
else
{

echo "<span class='adm_panel_span'><a href='?id=$post[id]&amp;act=up&amp;get_type=$get_type'>".lang('Выше')."</a> | ";
echo "<a href='?id=$post[id]&amp;act=down&amp;get_type=$get_type'>".lang('Ниже')."</a> | ";
echo "<a href='?id=$post[id]&amp;act=del&amp;'> ".lang('Удалить')."</a></span><br /><br />";

}

echo "  </td>";
echo "   </tr>";
}


echo "</table>";


if (isset($_GET['add']))
{
echo "<div class='p_m'>";
echo "<form action='?' method=\"post\">";
echo "Тип:<br />";
echo "<select name='type'>";
echo "<option value='inc'> ".lang('Виджет')."</option>";
echo "<option value='razd'> ".lang('Раздел')."</option>";
echo "</select><br />";

echo "Раздел:<br />";
echo "<select name='type_set'>";
echo "<option value='index'> ".lang('Кабинет')."</option>";
echo "<option value='settings'> ".lang('Настройки')."</option>";
echo "</select><br />";


echo lang('Название').":<br />";
echo "<input type=\"text\" name=\"name\" value=\"\"/><br />";

echo lang('Ссылка на виджет').":<br />";
echo "<input type=\"text\" name=\"url\" value=\"\"/><br />";


echo "<input class='submit' name='add' type='submit' value='".lang('Добавить')."' /><br />";
echo "<a href='?$passgen'>".lang('Отмена')."</a><br />\n";
echo "</form></div>";
}
else echo "<div class='foot'><a href='?add=$passgen'>".lang('Добавить пункт')."</a></div>";

echo "<div class='foot'>";
echo "&laquo;<a href='".APANEL."/'>".lang('В админку')."</a><br />";
echo "</div>";


include_once '../sys/inc/tfoot.php';
?>