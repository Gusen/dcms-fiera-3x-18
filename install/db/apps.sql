-- phpMyAdmin SQL Dump
-- version 4.3.4
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июл 03 2015 г., 03:31
-- Версия сервера: 5.6.22-log
-- Версия PHP: 5.5.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test_local`
--

-- --------------------------------------------------------

--
-- Структура таблицы `apps`
--

CREATE TABLE IF NOT EXISTS `apps` (
  `id` int(11) NOT NULL,
  `time_create` int(11) DEFAULT '0',
  `count_aut` int(11) DEFAULT '0',
  `opis` varchar(512) DEFAULT '',
  `link` varchar(99) DEFAULT '',
  `name` varchar(99) DEFAULT '',
  `name_lat` varchar(99) DEFAULT '',
  `img` varchar(32) DEFAULT 'default.png',
  `rating` varchar(512) DEFAULT '0.00'
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `apps`
--

INSERT DELAYED INTO `apps` (`id`, `time_create`, `count_aut`, `opis`, `link`, `name`, `name_lat`, `img`, `rating`) VALUES
(1, 1419014410, 0, 'Сотню лет ,ренигаты держали в страхе мир ,теперь власть в твоих руках!', '/pages/apps/renigads/', 'Ренигаты', 'RENIGADS', 'renigads.png', '0.00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `apps`
--
ALTER TABLE `apps`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `apps`
--
ALTER TABLE `apps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
